var guess = prompt("Guess My Number Game. Enter a number between 0 and 10: ", "");
var randomNum = Math.floor(Math.random() * 11);
var guessCount = 0;

while(guess !== randomNum) {
    if (guess == randomNum) {
        window.alert('You win!!!');
        break;
    }
    if (guess < randomNum) {
        guess = prompt('The number is too small');
    }
    if (guess > randomNum) {
        guess = prompt('The number is too big');
    }

    if (isNaN(guess)) {
        guess = prompt('Error!!!, must be a number. Example: 5');
    }

    guessCount++;
    if (guessCount === 3) {
        window.alert('Game Over!!!');
        break;
    }
    
}

