function shuffle (arr) {
    for (var i = 0; i < arr.length; i++) {
        var randomNum = Math.floor(Math.random() * arr.length);
        var tempName = "";
        var currentName = names[i];
        var randomName = names[randomNum];
        tempName = currentName;
        names[i] = randomName;
        names[randomNum] = tempName;
    }
    return arr; 
}

module.exports = {
    shuffle
}