var DB = [
    {
        genre:'thriller', 
        movies: [
            {
                title:'the usual suspects', release_date:1999
            }
        ]
    },
    {
        genre:'comedy', 
        movies: [
            {
                title:'pinapple express', release_date:2008
            }
        ]
    }
]

function moviesDB(DB, genre, movie) {
	if (DB.findIndex(genre) !== DB.genre.movie) {
		DB[movie] = genre;
	}
	if (DB.genre.movie == DB.genre.movie) {
		return 'the movie the usual suspects is already in the database!';
	}
}


module.exports ={
	moviesDB
}