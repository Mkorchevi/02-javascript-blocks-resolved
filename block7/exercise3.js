class BankAccount {
    constructor (amount = 0) {
        this.total = amount;
    }

    withdraw(value) {
        this.total -= value;
    }

    deposit(value) {
        this.total += value;
    }

    balance() {
        return this.total;
     }
}



module.exports = {
    BankAccount
}
