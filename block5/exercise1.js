function calc(arg1, arg2, arg3) {  
    if (arg3 == '+') {
        return arg1 + arg2;
    }
    if (arg3 == '-') {
        return arg1 - arg2;
    }
    if (arg3 == '*') {
        return arg1 * arg2;
    }
    if (arg3 == '/') {
        return arg1 / arg2;
    }
    else {
        return 'wrong data provided';
    }
}






module.exports = {
    calc
}