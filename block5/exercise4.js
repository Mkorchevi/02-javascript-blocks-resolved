function tellAge(month, day, year) {
    var today = new Date ();
    var birthday = new Date (`${month}/${day}/${year}`);
    var timeDiff = today - birthday;

    if( timeDiff < 1 ){
        
        return "You can't be born in the future!";
    }

    var days = Math.floor(timeDiff / (1000 * 60 * 60 * 24))
    if (days < 365) {

        var plu_sing = days > 1 ? 'days' : 'day';

        return 'You are ' + days  + ' ' + plu_sing + ' old';
    }

    if (days >= 365) {
        
        var years = Math.floor(days / 365);
        var plu_sing = years > 1 ? 'years' : 'year';

        return 'You are ' + years + ' ' + plu_sing + ' old';
    }
}




module.exports = {
    tellAge
}