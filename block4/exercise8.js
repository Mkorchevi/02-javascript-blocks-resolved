function uniqueElements(arr) {
    var newArr =[];
    for (var i = 0; i < arr.length; i++) {
        var ele = arr[i];
        if (!newArr.includes(arr[i])){
            newArr.push(ele);
        }
    }
    return 'old array' + ' ' + arr +',' + ' ' + 'new array' + ' ' + newArr;
}
   
module.exports = {
    uniqueElements
}