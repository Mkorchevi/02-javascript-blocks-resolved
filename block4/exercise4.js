function checker(arg) {
    var commas = 0;
    var question = 0;
       for (var i = 0; i < arg.length; i++) {
           if (arg[i] == ',') {
               commas++;
           }
           if (arg[i] == '?') {
               question++;
           }
       }
      var commaText = 'comma';
      var questionText = 'question mark';
      
        if (commas > 1) {
          commaText = 'commas';
         }
        
         if (question > 1) {
             questionText = 'question marks'
         }


    return commas + ' ' + commaText + ',' + ' ' + question + ' ' + questionText;
}




module.exports = {
    checker
}



