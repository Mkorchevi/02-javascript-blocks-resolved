function howManyCaps(arg) {
    var capitals = [];
    for (var i = 0; i < arg.length; i++) {      
        if (arg[i] == arg[i].toUpperCase() && arg[i] !== ' ') {
            capitals.push(arg[i]);
        }
    }
    return 'There are ' + capitals.length +  ' capitals ' + 'and these are ' + capitals;
}


module.exports = {
    howManyCaps
}