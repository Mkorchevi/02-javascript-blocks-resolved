function sumAll(obj) {
    if (obj == null || obj == undefined){
        return 0;
    }
    var values = Object.values(obj);
    var sum = values.reduce(function(total,ele) {
       return total+ele;
    },0);
    return sum;
}



module.exports = {
    sumAll
}