var movies = ['matrix','the dark knight','a beautiful mind','american pie']
function addToList (movie) {
    var movieList = [];
    for (var i = 0; i < movie.length; i++) {
        newObj = {};
        newObj['title'] = movie[i]; 
        newObj['id'] = i;
        movieList.push(newObj);
    }

    return movieList;
}

module.exports = {
    addToList
}