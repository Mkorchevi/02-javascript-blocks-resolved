
class bankAccount  {
    constructor (initial = 0) {
        this.total = initial;
    }

    withdraw(amount) {
        this.total -= amount;
    }

    deposit(amount) {
        this.total += amount;
    }

    balance() {
        return this.total;
     }
}

var b = new bankAccount();



module.exports = {
    bankAccount
}