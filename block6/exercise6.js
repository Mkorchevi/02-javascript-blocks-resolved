function getIndex (arr, key, value) {
    var index = -1;
    for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if (obj[key] == value) {
            return i;
        }
    }
    return index;
}




module.exports ={
    getIndex
}