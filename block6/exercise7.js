function runOnRange(obj) {
    var arr = [];
    var start = obj.start;
    var end   = obj.end;
    var step  = obj.step;
    if( start < end && step > 0){
        for( let i = start; i <= end; i+=step){
           arr.push(i);
        }
    }
    if(start > end && step < 0){
        for( let i = start; i >= end; i+=step){
           arr.push(i);
        }
    }
    return arr;
}

module.exports = {
    runOnRange
}