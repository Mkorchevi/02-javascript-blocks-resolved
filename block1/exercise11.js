var celsius = 39
var fahrenheit = 102
var toCelsius = function (fahrenheit){
    let toCelsius = (fahrenheit - 32) * (5) / (9);
    return Math.round(toCelsius)
}
var toFahr = function (celsius){
    let toFahr = (celsius / 5) * (9) + (32);
    return Math.round(toFahr)
}

module.exports = {
    celsius, 
    fahrenheit, 
    toCelsius, 
    toFahr,
}