var age=35, end_age=70, teas_day=2
var howManyTeas = function(age, end_age, teas_day){
    let howManyTeas = (end_age - age) * (teas_day * 365);
    return howManyTeas
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}