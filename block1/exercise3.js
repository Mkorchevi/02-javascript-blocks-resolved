var date_of_birth = 1982, future_year = 2018;
var ageCalc = function(date_of_birth, future_year){

	// your code goes here 
    let ageCalc = future_year - date_of_birth;
	// and return something :)
    return ageCalc
}

module.exports = {
    ageCalc,
    date_of_birth,
    future_year,
}